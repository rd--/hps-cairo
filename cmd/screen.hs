import Data.Cg.Minus.Types {- hcg-minus -}
import Graphics.Ps {- hps -}
import Graphics.Ps.Cairo {- hps-cairo -}

drawPath :: (Gs -> a -> b) -> R -> a -> b
drawPath m g p = m (greyGs g) p

drawCircle :: Pt R -> R -> Image
drawCircle p r =
  let c = arc p r 0 (2 * pi)
  in drawPath Stroke 0.2 c

drawRectangle :: Pt R -> R -> R -> R -> Image
drawRectangle p w h a =
  let r = rectangle p w h
  in drawPath Fill 0.4 (rotate a r)

drawText :: Pt R -> [Glyph] -> R -> Image
drawText p t n =
  let t' = Text (Font "Times" n) t
  in drawPath Stroke 0.2 (MoveTo p <> t')

showCoords :: Image
showCoords =
  let to_n n = [0 :: Int, 100 .. n]
      cs = [(x, y) | x <- to_n 800, y <- to_n 500]
      i = fromIntegral
      f (x, y) = drawText (Pt (i x) (i y)) (show (x, y)) 12
  in foldl1 Over (map f cs)

scena :: R -> [Image]
scena n =
  let p = Pt 250 250
      r = map (\a -> drawRectangle p 25 35 (pi / 12 * n * a)) [0 .. 4]
  in [ showCoords
     , drawCircle p (25 * n)
     , drawCircle p (65 * n)
     , drawText p "Some text" (24 * n)
     ]
      ++ r

main :: IO ()
main = do
  let p = Paper Pts 900 600
      i = scena 1.0
  ps "/tmp/test.ps" p i
  cg_pdf "/tmp/test.pdf" p i
