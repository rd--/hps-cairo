import Data.IORef {- base -}
import System.Random {- random -}

import Control.Monad.IO.Class {- transformers -}
import Data.List.Split {- split -}

import qualified Graphics.UI.Gtk as G {- gtk3 -}

import Data.Cg.Minus.Core {- hcg-minus -}
import Graphics.Ps {- hps -}
import Graphics.Ps.Cairo {- hps-cairo -}

-- type R = Double
type A =
  -- | Annular
  (R, R, R, R)
type E =
  -- | Element
  (A, R, R)

-- | Semi-annular.
semiann :: A -> R -> Image
semiann (gs, ir, xr, a) sa =
  let x = 250
      y = 250
      z = 250
      sa' = sa * 2.0 * pi
      a' = a * pi
      ir' = min ir xr
      xr' = max ir xr
      shift = translate x y . scale z z
  in Fill (greyGs gs) (shift (annular pt_origin ir' xr' sa' a'))

{- | Given seed /s/, generate /n/ random numbers in the range /(l,r)/.

> randn 1429837 2 1 2 == [1.2912518510129303,1.7155445332173258]
-}
randn :: Int -> Int -> R -> R -> [R]
randn s n l r =
  let f i = i * (r - l) + l
  in (map f . take n . randoms . mkStdGen) s

{- | Random initial settings for each element 'E' of animation, given
by annular segment 'A', phase increment 'R' and initial phase 'R'.
-}
settings :: [E]
settings =
  let g [a, b, c, d, e, f] = ((a, b, c, d), e, f)
      g _ = undefined
  in map g (chunksOf 6 (randn 0 66 0 1))

-- | Animation frame interval (in milliseconds).
frame_delay :: Int
frame_delay = 25

update :: G.DrawingArea -> IORef [R] -> IO Bool
update c r = do
  Just w <- G.widgetGetWindow c
  let ns = map (\(_, n, _) -> n / 100) settings
  modifyIORef r (zipWith (+) ns)
  aa <- readIORef r
  let as = map (\(a, _, _) -> a) settings
      i = foldl1 Over (zipWith semiann as aa)
  G.renderWithDrawWindow w (renderImage i)
  return True

main :: IO ()
main = do
  _ <- G.initGUI
  w <- G.windowNew
  c <- G.drawingAreaNew
  let is = map (\(_, _, i) -> i) settings
  r <- newIORef is
  G.set
    w
    [ G.windowTitle G.:= "hps"
    , G.windowResizable G.:= False
    ]
  G.widgetSetSizeRequest w 500 500
  _ <- G.on w G.keyPressEvent (liftIO (G.widgetDestroy w >> return True))
  _ <- G.on w G.destroyEvent (liftIO (G.mainQuit >> return True))
  _ <- G.on c G.exposeEvent (liftIO (update c r))
  _ <- G.timeoutAdd (liftIO (G.widgetQueueDraw w >> return True)) frame_delay
  G.set w [G.containerChild G.:= c]
  G.widgetShowAll w
  G.mainGUI
