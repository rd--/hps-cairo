-- | Cairo rendering for 'P.Image' from @hps@.
module Graphics.Ps.Cairo where

import Data.Cg.Minus.Types as Cg {- hcg-minus -}

import qualified Graphics.Ps as Ps {- hps -}

import qualified Graphics.Rendering.Cairo as C {- cairo -}
import qualified Graphics.Rendering.Cairo.Matrix as C {- cairo -}

applyMatrix :: Cg.Matrix Double -> C.Render ()
applyMatrix (Cg.Matrix a b c d e f) = C.transform (C.Matrix a b c d e f)

applyFont :: Ps.Font -> C.Render ()
applyFont (Ps.Font nm sz) = do
  C.selectFontFace nm C.FontSlantNormal C.FontWeightNormal
  C.setFontSize sz

renderPath :: Ps.Path -> C.Render ()
renderPath path =
  case path of
    Ps.MoveTo (Pt x y) -> C.moveTo x y
    Ps.LineTo (Pt x y) -> C.lineTo x y
    Ps.CurveTo (Pt x1 y1) (Pt x2 y2) (Pt x3 y3) -> C.curveTo x1 y1 x2 y2 x3 y3
    Ps.ClosePath (Pt x y) -> C.lineTo x y >> C.closePath
    Ps.Text f t -> applyFont f >> C.textPath t
    Ps.PTransform m p -> C.save >> applyMatrix m >> renderPath p >> C.restore
    Ps.Join p1 p2 -> renderPath p1 >> renderPath p2

setGs :: Ps.Gs -> C.Render ()
setGs (Ps.Gs (Ps.RGBA r g b a) w k j (d, d0) _) = do
  C.setSourceRGBA r g b a
  C.setLineWidth w
  C.setLineCap (lineCap k)
  C.setLineJoin (lineJoin j)
  C.setDash (map fromIntegral d) (fromIntegral d0)

-- | Render an 'Ps.Image' in cairo.
renderImage :: Ps.Image -> C.Render ()
renderImage image =
  case image of
    Ps.Empty -> return ()
    Ps.Stroke g p -> renderPath p >> setGs g >> C.stroke
    Ps.Fill g p -> renderPath p >> setGs g >> C.fillPreserve >> C.stroke
    Ps.ITransform m i -> C.save >> applyMatrix m >> renderImage i >> C.restore
    Ps.Over i1 i2 -> renderImage i1 >> renderImage i2

lineCap :: Ps.LineCap -> C.LineCap
lineCap c =
  case c of
    Ps.RoundCap -> C.LineCapRound
    Ps.ButtCap -> C.LineCapButt
    Ps.ProjectingSquareCap -> C.LineCapSquare

lineJoin :: Ps.LineJoin -> C.LineJoin
lineJoin j =
  case j of
    Ps.MiterJoin -> C.LineJoinMiter
    Ps.RoundJoin -> C.LineJoinRound
    Ps.BevelJoin -> C.LineJoinBevel

-- | The postscript model has the Y-axis ascending.
p_flip_y :: Int -> Ps.Path -> Ps.Path
p_flip_y x path =
  case path of
    Ps.Join p q -> Ps.Join (p_flip_y x p) (p_flip_y x q)
    Ps.Text f s ->
      let m = Cg.Matrix 1.0 0.0 0.0 1.0 0.0 (fromIntegral x)
      in Ps.PTransform m (Ps.Text f s)
    _ ->
      let m = Cg.Matrix 1.0 0.0 0.0 (-1.0) 0.0 (fromIntegral x)
      in Ps.PTransform m path

i_flip_y :: Int -> Ps.Image -> Ps.Image
i_flip_y x image =
  case image of
    Ps.Stroke g p -> Ps.Stroke g (p_flip_y x p)
    Ps.Fill g p -> Ps.Fill g (p_flip_y x p)
    Ps.ITransform m i -> Ps.ITransform m (i_flip_y x i)
    Ps.Over i j -> Ps.Over (i_flip_y x i) (i_flip_y x j)
    Ps.Empty -> Ps.Empty

-- | Render to PDF file, one image per page
cg_pdf :: FilePath -> Ps.Paper -> [Ps.Image] -> IO ()
cg_pdf fn p i_seq = do
  let (w, h) = Ps.paper_size_pt p
      (w', h') = (fromIntegral w, fromIntegral h)
      f s i = C.renderWith s (renderImage i >> C.showPage)
  C.withPDFSurface fn w' h' (\s -> mapM_ (f s) (map (i_flip_y h) i_seq))
