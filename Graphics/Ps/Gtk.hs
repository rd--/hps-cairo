module Graphics.Ps.Gtk where

import Data.Cg.Minus.Types as Cg {- hcg-minus -}

import qualified Graphics.Ps as Ps {- hps -}

import qualified Graphics.Rendering.Cairo as C {- cairo -}
import qualified Graphics.Rendering.Cairo.Matrix as C {- cairo -}

import qualified Graphics.UI.Gtk as G {- gtk3 -}

cg_gtk :: Ps.Paper -> [Ps.Image] -> IO ()
cg_gtk p i_seq = do
  let (w, h) = Ps.paper_size_pt p
  _ <- G.initGUI
  window <- G.windowNew
  canvas <- G.drawingAreaNew
  n <- newIORef 0
  G.set
    window
    [ G.windowTitle G.:= "hps"
    , G.windowResizable G.:= False
    ]
  G.widgetSetSizeRequest window w h
  _ <-
    G.on
      window
      G.keyPressEvent
      ( liftIO
          ( modifyIORef n (+ 1)
              >> G.widgetQueueDraw window
              >> return True
          )
      )
  _ <- G.on window G.destroyEvent (liftIO (G.mainQuit >> return True))
  _ <- G.on canvas G.exposeEvent (liftIO (updateCanvas n canvas i_seq))
  G.set window [G.containerChild G.:= canvas]
  G.widgetShowAll window
  G.mainGUI

updateCanvas :: (G.WidgetClass w) => IORef Int -> w -> [Ps.Image] -> IO Bool
updateCanvas n canvas pp = do
  Just window <- G.widgetGetWindow canvas
  k <- readIORef n
  G.renderWithDrawWindow window (renderImage (cycle pp !! k))
  return True

-- | Cairo rendering variant of 'Ps.ps'.
cg :: String -> Ps.Paper -> [Ps.Image] -> IO ()
cg _fn p i_seq = do
  let (_, h) = Ps.paper_size_pt p
  cg_gtk p (map (i_flip_y h) i_seq)
