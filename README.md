hps-cairo - cairo backend for hps

trivial cairo rendering, via hps

(c) rohan drape and others, 2006-2025
    gpl.2, http://gnu.org/copyleft/
    with contributions by declan murphy
    see darcs history for details
