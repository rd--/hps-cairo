all:
	echo "hps-cairo"

clean:
	rm -fR dist dist-newstyle *~

push-all:
	r.gitlab-push.sh hps-cairo

push-tags:
	r.gitlab-push.sh hps-cairo --tags

indent:
	fourmolu -i Graphics cmd

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Graphics/Ps/Cairo.hs
